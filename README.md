This document uses the [Markdown](http://daringfireball.net/projects/markdown/) syntax.

Gromacs_interactive is a workflow based on Decaf to steer a molecular dynamics simulation Gromacs. This framework provides a way to connect Gromacs to several visualization tools 
(FlowVR and VisIt). The simulation is coupled to a loop feedback tracking tracking a subset of atoms of the simulation and sending external forces to the simulation to make
the subset of atoms follow a path predefined by the user.

# Dependencies

- Decaf
- OpenGL
- FFTW
- Visit
- Damaris

Decaf is available on [bitbucket](https://bitbucket.org/tpeterka1/decaf/). 

OpenGL is required by VisIt as it uses Qt4:
```
sudo apt-get install freeglut3-dev # For Qt4 with OpenGL
```

FFTW can be compiled with the following instructions:
```
cd $HOME/software
wget http://www.fftw.org/fftw-3.3.6-pl2.tar.gz
tar -xf fftw-3.3.6-pl2.tar.gz
cd fftw-3.3.6-pl2
./configure --prefix $HOME/software/install/ -enable-shared --enable-float --enable-sse2
make -j4 install
```

VisIt offers several way to install it. We suggest to use the automatic script to install it:
```
# VisIt
cd $HOME/software
mkdir visit
cd visit
wget http://portal.nersc.gov/project/visit/releases/2.12.3/build_visit2_12_3
chmod a+rx build_visit2_12_3
./build_visit2_12_3 --prefix $HOME/software/install --parallel --qt5 --makeflags -j2
```
If you use an old compiler (gcc 4.X), remove the flag ```--qt5```.

Damaris provides the bridge between Decaf and VisIt. Damaris requires to install as well Xerces-C and XSD:

```
# XSD
cd $HOME/software
wget http://www.codesynthesis.com/download/xsd/4.0/linux-gnu/i686/xsd-4.0.0-i686-linux-gnu.tar.bz2
tar -xf xsd-4.0.0-i686-linux-gnu.tar.bz2
mv xsd-4.0.0-i686-linux-gnu/libxsd xsd-4.0.0-i686-linux-gnu/include

# Xerces
cd $HOME/software
wget http://mirrors.sonic.net/apache//xerces/c/3/sources/xerces-c-3.1.4.tar.gz
tar -xf xerces-c-3.1.4.tar.gz
cd xerces-c-3.1.4
./configure --prefix=$HOME/software/install --disable-threads --disable-network
make -j4 install

# Damaris
cd $HOME/software
wget https://gforge.inria.fr/frs/download.php/latestfile/3709/damaris-1.0.1.tgz
tar -xf damaris-1.0.1.tgz
mkdir damaris_build
cd damaris_build
```
WARNING: VisIt 2.12.3 is not detected automatically by Damaris. You need to edit the file 
```$HOME/software/damaris-1.0.1/cmake/Modules/FindVisIt.cmake``` in the sources of Damaris and add the version of VisIt you installed.
You can then compile Damaris as follow:
```
cmake ../damaris-1.0.1 -DCMAKE_INSTALL_PREFIX:PATH=$HOME/software/install -DXERCESC_ROOT:PATH=/$HOME/software/install -DENABLE_EXAMPLES:BOOL=FALSE -DVisIt_ROOT:PATH=$HOME/software/install -DXSD_ROOT:PATH=$HOME/software/xsd-4.0.0-i686-linux-gnu
```


# Project Compilation

Retrieve the sources of the project (in the current directory, e.g.):
```
git clone https://bitbucket.org/matthieu_dreher/gromacs_interactive.git .
```

The project is built using CMake. Assuming that you created a build directory, then:
```
cd path/to/project/build
```

The following command builds the project. We recommend saving this into a script and running the script:

```
cmake ../source/ \
-DCMAKE_INSTALL_PREFIX:PATH=$HOME/Apps/gromacs_interactive/install \
-DXERCESC_ROOT:PATH=$HOME/software/install \
-DDECAF_PREFIX:PATH=$HOME/Decaf/install \
-DVisIt_ROOT:PATH=$HOME/software/install \
-DDAMARIS_PREFIX:PATH=$HOME/software/install \
-DFFTW_PREFIX:PATH=$HOME/software/install
make -j4
make install
```

# Testing the installation

The project includes 1 molecular models to test the installation. The following commands run the tests:

```
cd /path/to/project/install
python gromacs_workflow_no_links.py
./wflow_gromacs.sh
```

The following output should appear in the terminal:
![alt text](images/steering1.png)

Once the workflow is running, it is possible to connect VisIt to visualize the current state of the simulation.
Open a new terminal, and launch VisIt:
![alt text](images/steering2.png)

Damaris/Viz creates .sim file in ```$HOME/.visit/simulations/``` used to connect VisIt to the running simulation. 
Click on Open then select the appropriate .sim file.
If you ran multiple simulations, the correct file to select is the last in the list:
![alt text](images/steering3.png)

If the connection was successful, the button Add should now be colored.
Now click on Add, select Pseudocolor, then click on space. The object space should then appear in the list below the button add.
![alt text](images/steering4.png)

We usually visualized the simulation using an isosurface. Click on Operators, select slicing, then Isosurface.
![alt text](images/steering5.png)

Finally you can visualize the simulation by pressing the button Draw:
![alt text](images/steering6.png)


# Simulation arguments

The project downloads, patches, and installs Gromacs 4.5.5 and Gromacs 4.6.7. Both versions of Gromacs have been modified 
to take into account several new parameters:

```
-[no]ddecaf  bool   yes     Activate Decaf operations
-nstepdecaf  int    500     Output decaf data every nstepdecaf iteration
```

These parameters control how frequently data are extracted from Gromacs by the framework. If -ddecaf is set to yes, the simulation will send data 
asynchronously to the workflow every nstepdecaf iterations. Note that this ddecaf should be set if the user want to steer or visualize the simulation.

# FlowVR Visualization
TODO

# Using another molecular model
TODO



