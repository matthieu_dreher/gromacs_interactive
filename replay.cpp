//---------------------------------------------------------------------------
//
// 2-node producer-consumer coupling example
//
// prod (4 procs) - con (2 procs)
//
// entire workflow takes 8 procs (2 dataflow procs between prod and con)
// this file contains the consumer (2 procs)
//
// Tom Peterka
// Argonne National Laboratory
// 9700 S. Cass Ave.
// Argonne, IL 60439
// mdreher@anl.gov
//
//--------------------------------------------------------------------------

#include <decaf/decaf.hpp>
#include <decaf/data_model/simplefield.hpp>
#include <decaf/data_model/arrayfield.hpp>
#include <decaf/data_model/blockfield.hpp>
#include <decaf/data_model/array3dconstructdata.hpp>
#include <boost/multi_array.hpp>
#include <decaf/data_model/boost_macros.h>

#include "decaf/data_model/morton.h"

#include <assert.h>
#include <math.h>
#include <mpi.h>
#include <map>
#include <cstdlib>
#include <sstream>
#include <fstream>
#include <boost/algorithm/string.hpp>

#include <decaf/workflow.hpp>

#include <sys/time.h>

#ifdef VISU
#include "Damaris.h"
#endif

using namespace decaf;
using namespace std;
using namespace boost;

double* rmesh_x =  NULL;
double* rmesh_y = NULL;
double* rmesh_z = NULL;

int* grid = NULL;
int filter = 0;

int DX,DY,DZ;

#define GRID(i,j,k) ( grid[i*DY*DZ + j*DZ + k] )


vector<pair<unsigned, double> > timings;

bool load_timings(MPI_Comm comm, string& basename, int start_rank, int end_rank)
{
    // Checking that we have the same level of parallelism
    int size, rank;
    MPI_Comm_size(comm, &size);
    MPI_Comm_rank(comm, &rank);

    int nb_files = end_rank - start_rank + 1;
    if(size != nb_files)
    {
        fprintf(stderr, "ERROR: ranks mismatch between the logs and the replay model. Expecting %d ranks.\n", nb_files);
        MPI_Abort(MPI_COMM_WORLD, -1);
    }

    int file_rank = start_rank + rank;
    stringstream ss;
    ss<<basename<<file_rank<<".csv";
    string filename = ss.str();
    fprintf(stderr, "Loading the file %s\n", filename.c_str());

    ifstream file(filename);
    string line;
    bool first_line = true;
    while(file.good())
    {
        getline(file, line);
        if(first_line)
        {
            first_line = false;
            continue;
        }
        std::vector<std::string> strs;
        boost::split(strs, line, boost::is_any_of("\n;"));

        if(line == "")
            continue;

        if(strs.size() != 5)
        {
            fprintf(stderr, "ERROR: Unable to parse the file properly.\n");
            MPI_Abort(MPI_COMM_WORLD, -1);
        }

        stringstream ssIt;
        ssIt.str(strs[0]);
        unsigned it;
        ssIt>>it;

        stringstream ssTime;
        ssTime.str(strs[2]);
        double timeVisit;
        ssTime>>timeVisit;

        //fprintf(stderr, "Reading It %d time %lf\n", it, timeVisit);
        timings.emplace_back(it, timeVisit);
    }

    fprintf(stderr, "Loaded %d timings.\n", timings.size());

    file.close();
    return true;

}

void print_timing()
{
    for(unsigned int i = 0; i < timings.size(); i++)
        fprintf(stderr, "It: %d, Sleep: %lf\n", timings[i].first, timings[i].second);
}

template <typename T>
T clip(const T& n, const T& lower, const T& upper) {
  return max(lower, min(n, upper));
}

unsigned int lineariseCoord(unsigned int x, unsigned int y, unsigned int z,
                            unsigned int dx, unsigned int dy, unsigned int dz)
{
    //return x * dy * dz + y * dz + z;
    //fprintf(stderr,"access to %u (%u %u %u)\n", x + y*dx + z*dx*dy,x, y, z);
    return x + y*dx + z*dx*dy;
}

unsigned int lineariseCoordCol(unsigned int x, unsigned int y, unsigned int z,
                            unsigned int dx, unsigned int dy, unsigned int dz)
{
    return z + y+dz + x*dz*dy;
}

std::set<int> filterIds; // Filter selecting the steered group

#define MAX_SIZE_REQUEST 2048
typedef struct
{
    int type;                   //Target absolute in space or relative to a reference point
    float target[3];            //Coordonates of the absolute target
    char targetRequest[2048];   //Request of the
}Target;

std::vector<Target> targets;

std::string model;

void updateGlobalBox(string& profile, BlockField& globalBox, float gridspace)
{
    vector<float> globalPos;
    vector<unsigned int> globalExtends(6);
    Block<3> *block = globalBox.getBlock();

    // 02_DA_W.3K
    if(profile.compare(std::string("SimplePeptideWater")) == 0)
    {
        globalPos = {
                        -0.7f, -0.72f, -0.8f,
                        29.0f, 45.0f, 25.4f
                    };
        filter = 191; // We keep only the water
    }
    else if(profile.compare(std::string("bench54k")) == 0)
    {
        globalPos = {
                        0.0f, 0.0f, 0.0f,
                        1283.0f, 1283.0f, 145.0f
                    };
    }
    else if(profile.compare(std::string("fepa")) == 0)
    {
        globalPos = {
                        -10.0f, -10.0f, -10.0f,
                        130.0f, 90.0f, 120.0f
                    };
        filter = 70000; // We keep everything, the Iron complex is
                        // at the end of the frame
    }
    else
    {
        std::cerr<<"ERROR : unknown profil, can't load a box"<<std::endl;
        exit(1);
    }

    // Extension of the box as these box are the strict box of the first frame
    // so even with some movements the model is still in the box

    float dX = ( globalPos[3] ) * 0.2f;
    float dY = ( globalPos[4] ) * 0.2f;
    float dZ = ( globalPos[5] ) * 0.2f;

    globalPos[0] -= dX;
    globalPos[1] -= dY;
    globalPos[2] -= dZ;
    globalPos[3] += 2*dX;
    globalPos[4] += 2*dY;
    globalPos[5] += 2*dZ;

    globalExtends[0] = 0;
    globalExtends[1] = 0;
    globalExtends[2] = 0;
    globalExtends[3] = (int)(globalPos[3] / gridspace) + 1;
    globalExtends[4] = (int)(globalPos[4] / gridspace) + 1;
    globalExtends[5] = (int)(globalPos[5] / gridspace) + 1;

    block->setGlobalBBox(globalPos);
    block->setGlobalExtends(globalExtends);
    block->setGridspace(gridspace);
    block->setGhostSizes(2);

    //globalBox.getBlock()->printBoxes();
    //globalBox.getBlock()->printExtends();

}

void loadTargets()
{
    if(model.compare(std::string("SimplePeptideWater")) == 0)
    {
        Target target;
        target.target[0] = 4.0;
        target.target[1] = 6.0;
        target.target[2] = 20.0;
        targets.push_back(target);

        target.target[0] = 23.0;
        target.target[1] = 6.0;
        target.target[2] = 20.0;
        targets.push_back(target);

        target.target[0] = 23.0;
        target.target[1] = 38.0;
        target.target[2] = 20.0;
        targets.push_back(target);

        target.target[0] = 4.0;
        target.target[1] = 38.0;
        target.target[2] = 20.0;
        targets.push_back(target);

        filterIds = {  109, 110, 111, 112, 113, 114,
                       115, 116, 117, 118, 119, 120
                    }; //HARD CODED for SimpleWater example
    }
    else if(model.compare(std::string("fepa")) == 0)
    {
        Target target;
        target.target[0] = 50.649998;
        target.target[1] = 40.020000;
        target.target[2] = 74.940002;
        targets.push_back(target);

        target.target[0] = 57.994247;
        target.target[1] = 42.744064;
        target.target[2] = 75.205559;
        targets.push_back(target);

        target.target[0] = 58.028599;
        target.target[1] = 39.480324;
        target.target[2] = 62.716755;
        targets.push_back(target);

        target.target[0] = 58.175446;
        target.target[1] = 36.721069;
        target.target[2] = 59.135941;
        targets.push_back(target);

        target.target[0] = 60.568310;
        target.target[1] = 35.987762;
        target.target[2] = 56.373985;
        targets.push_back(target);

        target.target[0] = 57.443069;
        target.target[1] = 41.200779;
        target.target[2] = 52.448627;
        targets.push_back(target);

        target.target[0] = 60.272179;
        target.target[1] = 41.596397;
        target.target[2] = 41.934307;
        targets.push_back(target);

        target.target[0] = 58.013557;
        target.target[1] = 49.347263;
        target.target[2] = 14.191130;
        targets.push_back(target);

        //Ids for ENT and FE residues
        for(int i = 69901; i <= 69952; i++)
        {
            filterIds.insert(i);
        }
        //filterIds.insert(69952);
    }


}

void posToFile(float* pos, int nbParticules, const string filename)
{
    ofstream file;
    cout<<"Filename : "<<filename<<endl;
    file.open(filename.c_str());

    unsigned int r,g,b;
    r = rand() % 255;
    g = rand() % 255;
    b = rand() % 255;

    unsigned int ur,ug,ub;
    ur = r;
    ug = g;
    ub = b;
    ur = clip<unsigned int>(ur, 0, 255);
    ug = clip<unsigned int>(ug, 0, 255);
    ub = clip<unsigned int>(ub, 0, 255);
    //cout<<"UColor : "<<ur<<","<<ug<<","<<ub<<endl;

    //cout<<"Number of particules to save : "<<nbParticules<<endl;
    file<<"ply"<<endl;
    file<<"format ascii 1.0"<<endl;
    file<<"element vertex "<<nbParticules<<endl;
    file<<"property float x"<<endl;
    file<<"property float y"<<endl;
    file<<"property float z"<<endl;
    file<<"property uchar red"<<endl;
    file<<"property uchar green"<<endl;
    file<<"property uchar blue"<<endl;
    file<<"end_header"<<endl;
    for(int i = 0; i < nbParticules; i++)
        file<<pos[3*i]<<" "<<pos[3*i+1]<<" "<<pos[3*i+2]
            <<" "<<ur<<" "<<ug<<" "<<ub<<endl;
    file.close();
}

void computeBBox(float* pos, int nbParticles,
                 float &xmin, float &xmax,
                 float &ymin, float &ymax,
                 float &zmin, float &zmax)
{
    if(nbParticles > 0)
    {
        xmin = pos[0];
        xmax = pos[0];
        ymin = pos[1];
        ymax = pos[1];
        zmin = pos[2];
        zmax = pos[2];

        for(int i = 1; i < nbParticles; i++)
        {
            if(xmin > pos[3*i])
                xmin = pos[3*i];
            if(ymin > pos[3*i+1])
                ymin = pos[3*i+1];
            if(zmin > pos[3*i+2])
                zmin = pos[3*i+2];
            if(xmax < pos[3*i])
                xmax = pos[3*i];
            if(ymax < pos[3*i+1])
                ymax = pos[3*i+1];
            if(zmax < pos[3*i+2])
                zmax = pos[3*i+2];
        }

        std::cout<<"["<<xmin<<","<<ymin<<","<<zmin<<"]["<<xmax<<","<<ymax<<","<<zmax<<"]"<<std::endl;
    }
}

void compteBBox(float* pos, int nbPos)
{
    if(pos != NULL && nbPos > 0)
    {
        float xmin,ymin,zmin,xmax,ymax,zmax;
        xmin = pos[0];
        ymin = pos[1];
        zmin = pos[2];
        xmax = pos[0];
        ymax = pos[1];
        zmax = pos[2];

        for(int i = 0; i < nbPos; i++)
        {
            if(pos[3*i] < xmin)
                xmin = pos[3*i];
            if(pos[3*i+1] < ymin)
                ymin = pos[3*i+1];
            if(pos[3*i+2] < zmin)
                zmin = pos[3*i+2];
            if(pos[3*i] > xmax)
                xmax = pos[3*i];
            if(pos[3*i+1] > ymax)
                ymax = pos[3*i+1];
            if(pos[3*i+2] > zmax)
                zmax = pos[3*i+2];
        }

        fprintf(stderr, "Local bounding box : [%f %f %f] [%f %f %f]\n", xmin,ymin,zmin,xmax,ymax,zmax);
        fprintf(stderr, "Global bounding box : [%f %f %f] [%f %f %f]\n", -0.7f, -0.72f, -0.8f,
                29.0f, 45.0f, 25.4f);
    }
}

void updateGrid(int* grid, int x, int y, int z, unsigned int DX, unsigned int DY, unsigned int DZ, int increment)
{
    for(int i = -1; i < 2; i++)
    {
        for(int j = -1; j < 2; j++)
        {
            for(int k = -1; k < 2; k++)
            {
                int localX = x+i;
                int localY = y+j;
                int localZ = z+k;
                if(localX >= 0 && localX < DX && localY >= 0 && localY < DY && localZ >= 0 && localZ < DZ)
                    grid[lineariseCoord(localX,localY,localZ,DX,DY,DZ)] += increment;
            }
        }
    }
}

pConstructData computeMorton(MPI_Comm comm, pConstructData in_data, string& model, float gridspace)
{
    struct timeval beginIt;
    struct timeval endIt;
    gettimeofday(&beginIt, NULL);

    int comm_rank;
    int comm_size;
    MPI_Comm_rank(comm, &comm_rank);
    MPI_Comm_size(comm, &comm_size);

    //TODO : find a way to pass the value as a global argument
    BlockField globalBox(true);
    updateGlobalBox(model, globalBox, gridspace);

    // We compute the morton codes which will be used
    // for the block redistribution and density grid

    ArrayFieldf posArray = in_data->getFieldData<ArrayFieldf>("pos");
    if(!posArray)
    {
        fprintf(stderr, "ERROR dflow: unable to find the field required \"pos\" in the data model.\n");
        MPI_Abort(MPI_COMM_WORLD, -1);
    }


    float* pos = posArray.getArray();
    int nbParticle = posArray->getNbItems();

    //compteBBox(pos, nbParticle);

    vector<float> filteredPos;
    vector<unsigned int> filteredIds;

    ArrayFieldu indexes = in_data->getFieldData<ArrayFieldu>("ids");
    if(!indexes)
    {
        fprintf(stderr, "ERROR dflow: unable to find the field required \"ids\" in the data model.\n");
        MPI_Abort(MPI_COMM_WORLD, -1);
    }

    SimpleFieldi itField = in_data->getFieldData<SimpleFieldi>("it");

    unsigned int* index = indexes.getArray();

    int nbFilteredPart = 0;
    for(int i = 0; i < nbParticle; i++)
    {
        if(index[i] < filter)
        {
            filteredPos.push_back(pos[3*i] * 10.0); // Switching from nm to Angstrom
            filteredPos.push_back(pos[3*i+1] * 10.0);
            filteredPos.push_back(pos[3*i+2] * 10.0);
            filteredIds.push_back(index[i]);
            nbFilteredPart++;
        }
    }

    ArrayFieldf filterPosField = ArrayFieldf(&filteredPos[0], 3 * nbFilteredPart, 3, false);
    ArrayFieldu filterIdsField = ArrayFieldu(&filteredIds[0], nbFilteredPart, 1, false);

    vector<unsigned int> morton(nbFilteredPart);
    float *box = globalBox.getBlock()->getGlobalBBox();
    unsigned int* cells = globalBox.getBlock()->getGlobalExtends();
    unsigned int offset = 0;

    //fprintf(stderr,"Number of particles: %i\n", nbFilteredPart);

    for(int i = 0; i < nbFilteredPart; i++)
    {
        //Using cast from float to unsigned int to keep the lower int
        unsigned int cellX = (unsigned int)((filteredPos[3*i] - box[0]) / gridspace);
        unsigned int cellY = (unsigned int)((filteredPos[3*i+1] - box[1]) / gridspace);
        unsigned int cellZ = (unsigned int)((filteredPos[3*i+2] - box[2]) / gridspace);

        //Clamping the cells to the bbox. Atoms can move away from the box, we count them in the nearest cell (although it's not correct)
        cellX = cellX >= (cells[3])?(cells[3]-1):cellX;
        //cellX = cellX < 0?0:cellX;
        cellY = cellY >= (cells[4])?(cells[4]-1):cellY;
        //cellY = cellY < 0?0:cellY;
        cellZ = cellZ >= (cells[5])?(cells[5]-1):cellZ;
        //cellZ = cellZ < 0?0:cellZ;

        //Computing the corresponding morton code
        morton[offset] = Morton_3D_Encode_10bit(cellX,cellY,cellZ);
        offset++;
    }

    ArrayFieldu mortonField = ArrayFieldu(&morton[0], nbFilteredPart, 1, false);

    pConstructData container;
    container->appendData("pos", filterPosField,
                          DECAF_POS, DECAF_PRIVATE,
                          DECAF_SPLIT_DEFAULT, DECAF_MERGE_APPEND_VALUES);
    container->appendData("domain_block", globalBox,
                          DECAF_NOFLAG, DECAF_SHARED,
                          DECAF_SPLIT_KEEP_VALUE, DECAF_MERGE_DEFAULT);
    container->appendData("morton", mortonField,
                          DECAF_MORTON, DECAF_PRIVATE,
                          DECAF_SPLIT_DEFAULT, DECAF_MERGE_APPEND_VALUES);
    container->appendData("ids", filterIdsField,
                          DECAF_NOFLAG, DECAF_PRIVATE,
                          DECAF_SPLIT_DEFAULT, DECAF_MERGE_APPEND_VALUES);
    container->appendData("it", itField,
                          DECAF_NOFLAG, DECAF_SHARED,
                          DECAF_SPLIT_KEEP_VALUE, DECAF_MERGE_FIRST_VALUE);

    // Exchanging the data
    RedistBlockMPI* redist = new RedistBlockMPI(0, comm_size, 0, comm_size, comm);
    redist->process(container, DECAF_REDIST_SOURCE);

    pConstructData result;
    redist->process(result, DECAF_REDIST_DEST);
    redist->flush();

    gettimeofday(&endIt, NULL);

    return result;

    /*double elapsedTimeIt = (endIt.tv_sec - beginIt.tv_sec) * 1000.0;      // sec to ms
    elapsedTimeIt += (endIt.tv_usec - beginIt.tv_usec) / 1000.0;   // us to ms

    stats<<iteration;
    stats<<";"<<elapsedTimeIt;
    stats<<std::endl;
    stats.flush();

    iteration++;*/
}

// consumer
void treatment1(Decaf* decaf)
{
    vector< pConstructData > in_data;
    fprintf(stderr, "Launching treatment\n");
    fflush(stderr);

    int rank;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    int iteration = 0;

    //loadTargets();

    bool hasWrite = false;

    std::ofstream stats;
    std::string filename = "replay_";
    filename.append(std::to_string(rank));
    filename.append(".csv");

    std::cerr<<"Opening filename :"<<filename<<std::endl;
    stats.open(filename);
    stats<<"It;elapsedIt;elapsedVisit;RatioVisit"<<std::endl;

    struct timeval beginIt;
    struct timeval endIt;
    struct timeval beginVisit;
    struct timeval endVisit;
    struct timeval beginRedist;
    struct timeval endRedist;

    int current_timing = 0;

    //print_timing();

    while (decaf->get(in_data))
    {

        gettimeofday(&beginIt, NULL);

        // Exchaning the the data
        gettimeofday(&beginRedist, NULL);
        string model = string("fepa");
        pConstructData redist_data = computeMorton(decaf->local_comm_handle(), in_data[0], model, 1.0);
        gettimeofday(&endRedist, NULL);

        // get the atom positions
        if(iteration == 0 && !redist_data->hasData("domain_block"))
        {
            fprintf(stderr,"ERROR : No block info in the data model\n");
            MPI_Abort(MPI_COMM_WORLD, 0);
        }


        SimpleFieldi itField = redist_data->getFieldData<SimpleFieldi>("it");
        if(itField)
            iteration = itField.getData();


        //boost::multi_array<float, 3>* boostGrid = NULL;
        if(redist_data->hasData("domain_block"))
        {
            // Getting the grid info
            BlockField blockField  = redist_data->getFieldData<BlockField>("domain_block");
            Block<3>* block = blockField.getBlock();

            // Now each process has a sub domain of the global grid
            // Building the grid
            unsigned int* lExtends = block->getLocalExtends();
            if(!grid)
                grid = new int[lExtends[3]*lExtends[4]*lExtends[5]];
            bzero(grid, lExtends[3]*lExtends[4]*lExtends[5]*sizeof(int));

            //boost::multi_array<float, 3>* boostGrid = new boost::multi_array<float, 3>(boost::extents[lExtends[3]][lExtends[4]][lExtends[5]]);

            ArrayFieldu mortonField = redist_data->getFieldData<ArrayFieldu>("morton");
            if(mortonField)
            {
                unsigned int *morton = mortonField.getArray();
                int nbMorton = mortonField->getNbItems();
                ArrayFieldf posField = redist_data->getFieldData<ArrayFieldf>("pos");
                float* pos = posField.getArray();

                ArrayFieldu idsField = redist_data->getFieldData<ArrayFieldu>("ids");
                unsigned int* ids = idsField.getArray();

                for(int i = 0; i < nbMorton; i++)
                {

                    // Building the density grid for the PATH finding
                    // No filtering for this grid as it's not for visualization
                    unsigned int x,y,z;
                    Morton_3D_Decode_10bit(morton[i], x, y, z);

                    int localx, localy, localz;
                    localx = x - lExtends[0];
                    localy = y - lExtends[1];
                    localz = z - lExtends[2];

                    //Building the density grid for visualization
                    //if(filterIds.count(ids[i]) == 0)
                    //    continue;
                    //HARD CODED FOR FEPA
                    //17432 for the channel
                    //69901 for the iron complex
                    if(ids[i] > 17432 && ids[i] < 69901)
                        continue;


                    // DEBUG: Checking if the particle should be here
                    /*if(!block->isInLocalBlock(x,y,z))
                    {
                        fprintf(stderr, "ERROR : particle not belonging to the local block. FIXME\n");
                        fprintf(stderr, "Particle : %f %f %f\n", pos[3*i], pos[3*i+1], pos[3*i+2]);
                        fprintf(stderr, "Particle : %u %u %u\n", x, y, z);
                        if(block->isInLocalBlock(pos[3*i], pos[3*i+1], pos[3*i+2]))
                            fprintf(stderr, "The particle is in the position block\n");
                        else
                            fprintf(stderr, "The particle is not in the position block\n");

                        float gridspace = blockField.getBlock()->getGridspace();
                        float *box = blockField.getBlock()->getGlobalBBox();
                        //Using cast from float to unsigned int to keep the lower int
                        unsigned int cellX = (unsigned int)((pos[3*i] - box[0]) / gridspace);
                        unsigned int cellY = (unsigned int)((pos[3*i+1] - box[1]) / gridspace);
                        unsigned int cellZ = (unsigned int)((pos[3*i+2] - box[2]) / gridspace);

                        fprintf(stderr," Particle after recomputation : %u %u %u\n", cellX, cellY, cellZ);
                    }*/

                    updateGrid(grid, localx,localy,localz,lExtends[3],lExtends[4],lExtends[5],1);
                }
            }

            int counter = 0;
            for(unsigned int i = 0; i < lExtends[3]*lExtends[4]*lExtends[5]; i++)
            {
                if(grid[i] > 0)
                    counter++;
            }

            //fprintf(stderr, "Number of active cells : %i\n", counter);
            //fprintf(stderr, "Total number of cells : %u\n", (lExtends[3]*lExtends[4]*lExtends[5]));


            //fprintf(stderr,"Computation of the grid completed\n");
#ifdef VISU
            // Normal data transmition to Damaris
            gettimeofday(&beginVisit, NULL);
            if(current_timing < timings.size())
            {
                unsigned timing_it = timings[current_timing].first;
                //fprintf(stderr, "Current_timing: %d Timing_it: %d, iteration: %d\n", current_timing, timing_it, iteration);
                if(timing_it < iteration)
                {
                    // Case where we did not record yet. Not doing anything
                }
                else if(timing_it == iteration)
                {
                    double timing_time = timings[current_timing].second * 1000.0; //us
                    unsigned sleep_time = (unsigned)timing_time;
                    //fprintf(stderr, "Sleeping %lf %d\n", timing_time, sleep_time);
                    usleep(sleep_time);
                    current_timing++;
                }
                else
                {
                    while(timing_it != iteration)
                    {
                        current_timing++;
                        if(current_timing >= timings.size()) break;
                        timing_it = timings[current_timing].first;
                    }

                    if(timing_it == iteration)
                    {
                        double timing_time = timings[current_timing].second * 1000.0; //us
                        unsigned sleep_time = (unsigned)timing_time;
                        //fprintf(stderr, "Sleeping %lf %d\n", timing_time, sleep_time);
                        usleep(sleep_time);
                        current_timing++;
                    }
                }
            }
            gettimeofday(&endVisit, NULL);
#endif
        }


        //decaf->put(in_data[0]);

        //if(boostGrid) delete boostGrid;

        gettimeofday(&endIt, NULL);

        double elapsedTimeIt = (endIt.tv_sec - beginIt.tv_sec) * 1000.0;      // sec to ms
        elapsedTimeIt += (endIt.tv_usec - beginIt.tv_usec) / 1000.0;   // us to ms
        double elapsedTimeVisit = (endVisit.tv_sec - beginVisit.tv_sec) * 1000.0;      // sec to ms
        elapsedTimeVisit += (endVisit.tv_usec - beginVisit.tv_usec) / 1000.0;   // us to ms
        double elapsedTimeRedist = (endRedist.tv_sec - beginRedist.tv_sec) * 1000.0;      // sec to ms
        elapsedTimeRedist += (endRedist.tv_usec - beginRedist.tv_usec) / 1000.0;   // us to ms

        fprintf(stderr,"Iteration %i: It: %f, Visit: %f, Ratio: %f\n", iteration, elapsedTimeIt, elapsedTimeVisit, (elapsedTimeVisit/elapsedTimeIt)*100.0);
        stats<<iteration;
        stats<<";"<<elapsedTimeIt;
        stats<<";"<<elapsedTimeVisit;
        stats<<";"<<(elapsedTimeVisit/elapsedTimeIt)*100.0;
        stats<<";"<<elapsedTimeRedist;
        stats<<std::endl;
        stats.flush();
        iteration++;
    }

    // Cleaning the grid
    delete [] rmesh_x;
    delete [] rmesh_y;
    delete [] rmesh_z;

    delete [] grid;

    stats.close();

    // terminate the task (mandatory) by sending a quit message to the rest of the workflow
    fprintf(stderr, "Treatment terminating\n");
    decaf->terminate();
}

// every user application needs to implement the following run function with this signature
// run(Workflow&) in the global namespace
void run(Workflow& workflow, string& basename, int start_rank, int end_rank)                             // workflow
{
    MPI_Init(NULL, NULL);

    char processorName[MPI_MAX_PROCESSOR_NAME];
    int size_world, rank, nameLen;

    MPI_Comm_size(MPI_COMM_WORLD, &size_world);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Get_processor_name(processorName,&nameLen);

    srand(time(NULL) + rank * size_world + nameLen);

    fprintf(stderr, "treatment rank %i\n", rank);

    // create decaf
    Decaf* decaf = new Decaf(MPI_COMM_WORLD, workflow);
#ifdef VISU
    //Initalizing the Damaris context
    damaris_initialize("decaf_grid.xml",decaf->con_comm_handle());

    int is_client, err;
    err = damaris_start(&is_client);

    if((err == DAMARIS_OK || err == DAMARIS_NO_SERVER) && is_client) {
        MPI_Comm damaris_com;
        damaris_client_comm_get(&damaris_com);

        int decaf_comm_size, damaris_comm_size;
        MPI_Comm_size(damaris_com, &damaris_comm_size);
        MPI_Comm_size(decaf->con_comm_handle(), &decaf_comm_size);
        if(decaf_comm_size != damaris_comm_size)
        {
            fprintf(stderr, "ERROR : Damaris configured to use helper cores.");
            fprintf(stderr, "Set cores and nodes to 0 in decaf_grid.xml\n");
            MPI_Abort(MPI_COMM_WORLD, 0);
        }
    }
    else
    {
        fprintf(stderr, "ERROR during the initialization of Damaris. Abording.\n");
        MPI_Abort(MPI_COMM_WORLD, 0);
    }
#endif

    load_timings(decaf->con_comm_handle(), basename, start_rank, end_rank);

    // start the task
    treatment1(decaf);
#ifdef VISU
    damaris_stop();
#endif
    // cleanup
    delete decaf;
    fprintf(stderr,"Decaf deleted. Waiting on finalize\n");
    MPI_Finalize();
}

// test driver for debugging purposes
// normal entry point is run(), called by python
int main(int argc,
         char** argv)
{
    fprintf(stderr, "Hello replay\n");

    if(argc != 5)
    {
        fprintf(stderr, "Usage : replay profile basename first_rank last_rank\n");
        exit(0);
    }

    model = string(argv[1]);
    string basename = string(argv[2]);
    int first_rank = atoi(argv[3]);
    int last_rank = atoi(argv[4]);

    // define the workflow
    Workflow workflow;
    Workflow::make_wflow_from_json(workflow, "wflow_gromacs.json");


    // run decaf
    run(workflow, basename, first_rank, last_rank);

    return 0;
}
