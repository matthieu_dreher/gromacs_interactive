# a small 2-node example, just a producer and consumer

# --- include the following 4 lines each time ---

import networkx as nx
import os
import imp
wf = imp.load_source('workflow', os.environ['DECAF_PREFIX'] + '/python/workflow.py')

# --- set your options here ---

# path to .so module for dataflow callback functions
#mod_path = os.environ['DECAF_PREFIX'] + '/examples/gromacs/libmod_dflow_gromacs.so'
mod_path = os.getcwd() + '/bin/libmod_dflow_gromacs.so'

# define workflow graph
# 2-node workflow
#
#    prod (4 procs) -> con (2 procs)
#
#  entire workflow takes 8 procs (2 dataflow procs between producer and consumer)
#  dataflow can be overlapped, but currently all disjoint procs (simplest case)

w = nx.DiGraph()
w.add_node("gmx", start_proc=0, nprocs=4, func='gmx', cmdline='./gromacs-4.5/bin/mdrun_mpi_4.5.5_decaf -s /home/matthieu/INRIA/molecules/FEPA/holo314.tpr -v', tokens=1)
w.add_node("treatment",  start_proc=6, nprocs=2, func='treatment', cmdline='./bin/treatment fepa')
w.add_node("target",  start_proc=8, nprocs=1, func='target', cmdline='./bin/targetmanager fepa 5.0 0.5')
w.add_edge("gmx", "treatment", start_proc=4, nprocs=2, func='dflow_morton_fepa', path=mod_path,
           prod_dflow_redist='proc', dflow_con_redist='block', cmdline='./bin/dflow_gromacs')
w.add_edge("treatment", "target", start_proc=0, nprocs=0, func='dflow_simple', path=mod_path,
           prod_dflow_redist='proc', dflow_con_redist='proc', cmdline='./bin/dflow_gromacs')
w.add_edge("target", "gmx", start_proc=0, nprocs=0, func='dflow_simple', path=mod_path,
           prod_dflow_redist='proc', dflow_con_redist='proc', cmdline='./bin/dflow_gromacs')

# --- convert the nx graph into a workflow data structure and run the workflow ---

#wf.workflowToJson(w, mod_path, "wflow_gromacs.json")
#wf.workflowToSh(w, "wflow_gromacs.sh")
wf.processGraph(w, "wflow_gromacs")
