# a small 2-node example, just a producer and consumer

# --- include the following 4 lines each time ---

import networkx as nx
import os
import imp
import argparse

wf = imp.load_source('workflow', os.environ['DECAF_PREFIX'] + '/python/workflow.py')

# --- set your options here ---

# path to .so module for dataflow callback functions
mod_path = '/home/matthieu/Argonne/Decaf/source_froggy/decaf_app/gromacs_interactive/install/bin/libmod_dflow_gromacs.so'

parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
wf.initParserForTopology(parser)
args = parser.parse_args()

# Creating the topology
topo = wf.topologyFromArgs(args)
subtopos = topo.splitTopology(["gmx","dflow","grid"],[4,2,2])

w = nx.DiGraph()
w.add_node("gmx", topology=subtopos[0], func='gmx', cmdline='./gromacs-4.5/bin/mdrun_mpi_4.5.5_decaf -s /home/matthieu/INRIA/molecules/FEPA/holo314.tpr -v')
w.add_node("grid",  topology=subtopos[1], func='treatment', cmdline='./bin/grid fepa')
w.add_edge("gmx", "grid", topology=subtopos[2], func='dflow_morton_fepa', path=mod_path,
           prod_dflow_redist='proc', dflow_con_redist='block', cmdline='./bin/dflow_gromacs')

# --- convert the nx graph into a workflow data structure and run the workflow ---

wf.processGraph(w, "wflow_gromacs")
